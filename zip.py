# import os
# arr = os.listdir()
# thisdir = os.getcwd()

# print(arr)

# importing required modules 
from zipfile import ZipFile 
  
# specifying the zip file name 
# file_name = "my_python_files.zip"
  
# # opening the zip file in READ mode 
# with ZipFile(file_name, 'r') as zip: 
#     # printing all the contents of the zip file 
#     zip.printdir() 
  
#     # extracting all the files 
#     print('Extracting all the files now...') 
#     zip.extractall() 
#     print('Done!')
with ZipFile('oanda_update_candles.zip', 'w') as zipObj2:
   # Add multiple files to the zip
   zipObj2.write('package-lock.json')
   zipObj2.write('package.json')
   zipObj2.write('index.js')
   zipObj2.write('at202xcloud-e8a6266f9a57.json')
   
