const axios = require("axios");
const admin = require("firebase-admin");
const serviceAccount = require("./at202xcloud-e8a6266f9a57.json");

admin.initializeApp({ credential: admin.credential.cert(serviceAccount) });

const db = admin.firestore();




db.collection("BROKER").doc("DATA").get().then((res) => {
    
    const workers = {
      instruments: null,
      time_frames: null,
      candles_count: null,
    };
    console.log(res.data())
    workers["instruments"] = res.data().instruments
    workers["time_frames"] = res.data().time_frames
    workers["candles_count"] = res.data().candles_count

    const options = {
        method: "get",
        headers: {
          "Content-Type": "application/json",
          Authorization:
            "Bearer 1d5a4136d5279b04d815c7f736b50ea1-60e8d9e1d1ff67ba601219754891ff5f",
        },
      };
  workers.instruments.map(instrument => {
    workers.time_frames.map(time_frame => {
      axios
        .get(
		  `https://api-fxpractice.oanda.com/v3/instruments/${instrument}/candles?count=${workers.candles_count}&price=M&granularity=${time_frame}`, options
        )
        .then(async function (response) {
          const dataRef = db
            .collection("CANDLES")
            .doc(instrument)
            .collection(time_frame)
            .doc("DATA");

          await dataRef.set(response.data);
        })
        .catch(function (error) {
          console.log(error, " err");
        });
    });
  });
})



